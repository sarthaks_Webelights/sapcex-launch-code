
import LaunchDashboardHeader from '../../Component/LaunchDashboard/LaunchDashboardHeader';
import LaunchTable from '../../Component/LaunchDashboard/LaunchTable';

import './style.css'

const LaunchDashboard = () => {

  return (
    <>
      <LaunchDashboardHeader /> 
      <div className="dashboard-container">
        <div className="dashboard-table">
          <LaunchTable />
        </div>
      </div>
    </>
  );
}

export default LaunchDashboard;