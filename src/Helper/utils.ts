export const constantsString = {
    number : "No:",
    launchedUTC: "Launched (UTC)",
    location: "Location",
    mission: "Mission",
    orbit: "Orbit",
    launchStatus: "Launch Status",
    rocket: "Rocket"
}

export const launchStatusFilter = {
    all: 'All Launches',
    failed: 'Failed Launches',
    success: 'Successfull Launches'
}

export const launchDateFilter = {
    all: "Showing All Records",
    "2019": '2019',
    "2018": '2018',
    "2017": '2017',
}