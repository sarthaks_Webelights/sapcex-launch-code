import { useCallback, useEffect, useMemo, useState } from 'react';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import moment from 'moment';
import { Chip } from '@mui/material';
import { TablePagination } from '@mui/base';
import { useMutation } from "@tanstack/react-query";
import Pagination from '@mui/material/Pagination';

import TableRowsLoaderSkeleton from './TableRowsLoaderSkeleton'
import TableFilter from './TableFilter';

import { constantsString } from '../../Helper/utils'

import { getAllSpacexLaunch } from '../../Service/getLaunches'

import './launch-table.css'

export default function LaunchTable() {

  const { isLoading, data, mutate } = useMutation(['SpacexLaunchData'], (filter: string) => getAllSpacexLaunch(filter))

  const GetLaunchStatus = useCallback((launchDetails: any)=>{
    if(launchDetails.upcoming){
      return <Chip color='info' label="Upcoming" /> 
    } else if(launchDetails.launch_success){
      return <Chip color='success' label="Success" /> 
    } else {
      return <Chip color="error" label= "Failed" /> 
    }
  }, [])

  const[filter, setFilter] = useState({
    launchStatus: "all",
    launchDateFilter: "all"
  })

  useEffect(()=>{
    mutate("")
  },[mutate])

  const queryParams = new URLSearchParams(window.location.search)
  const launch_year = queryParams.get("launch_year")
  const launch_success = queryParams.get("launch_success")
  
  useEffect(()=>{
    let queryFilterData ={
      launchDateFilter: "",
      launchStatus: ""
    };
    queryFilterData.launchDateFilter= launch_year ? launch_year : "all" ;
    
    queryFilterData.launchStatus = launch_success ? launch_success === "true" ? "success" : "failed" : "all";
    handleFilter(queryFilterData)
  },[launch_year, launch_success])

  const [page, setPage] = useState(0);
  const [rowsPerPage] = useState(10);

  const handleFilter = useCallback((filter:{
    launchStatus: string
    launchDateFilter: string
}) => {
  let filtersStack =[]
  setFilter(filter);
  if(!(filter.launchDateFilter === "all")){
    filtersStack.push(`launch_year=${filter.launchDateFilter}`)
  }
  if(!(filter.launchStatus === "all")){
    filtersStack.push(`launch_success=${filter.launchStatus === "success" ? true : false}`)
  }
  window.history.replaceState("", "",`/?${filtersStack.join("&")}`);
  mutate(`${filtersStack.join("&")}`)
  }, []); 

  const launchData = useMemo(() => {
    return data?.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((row: any, index: number) => (row))
  }, [ data, page, rowsPerPage ]) 

  return (
    <>
    <TableFilter 
      filter ={filter}
      setFilter ={setFilter}
      handleFilter={handleFilter}
    />
    <TableContainer component={Paper}>
      <Table sx={{ minWidth: 650 }} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell>{constantsString.number}</TableCell>
            <TableCell key={constantsString.launchedUTC} align="right">{constantsString.launchedUTC}</TableCell>
            <TableCell key={constantsString.location} align="right">{constantsString.location}</TableCell>
            <TableCell key={constantsString.mission} align="right">{constantsString.mission}</TableCell>
            <TableCell key={constantsString.orbit} align="right">{constantsString.orbit}</TableCell>
            <TableCell key={constantsString.launchStatus} align="right">{constantsString.launchStatus}</TableCell>
            <TableCell key={constantsString.rocket} align="right">{constantsString.rocket}</TableCell>
          </TableRow>
        </TableHead>
        
        <TableBody>
        {isLoading ? 
          <TableRowsLoaderSkeleton rowsNum={10} /> : 
          launchData?.map((row: any) => (
          <TableRow
            key={row.name}
            sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
          >
            <TableCell component="th" scope="row">
              {row.flight_number}
            </TableCell>
            <TableCell align="right">{moment(row.launch_date_utc).add(1, 'day').format('LLL')}</TableCell>
            <TableCell align="right">{row.launch_site.site_name}</TableCell>
            <TableCell align="right">{row.mission_name}</TableCell>
            <TableCell align="right">{row.rocket.second_stage.payloads[0].orbit}</TableCell>
            <TableCell align="right">{GetLaunchStatus(row)}</TableCell>
            <TableCell align="right">{row.rocket.rocket_name}</TableCell>
          </TableRow>
        ))}
        </TableBody>
      </Table>
    </TableContainer>
    {launchData?.length ? null : 
      <div className="no-record-container">
        <Chip label="No Record Found" />
    </div>}
    <div className='pagination-container'>
      <Pagination 
        count={Math.floor(data?.length / rowsPerPage)} 
        defaultPage={page} 
        onChange={(_: React.ChangeEvent<unknown>, pageNum: number)=>{
          setPage(pageNum)
        }}
        variant="outlined" 
        shape="rounded"
      />
    </div>
  </>
  );
}

