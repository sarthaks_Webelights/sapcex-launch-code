import * as React from 'react';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select, { SelectChangeEvent } from '@mui/material/Select';

import { launchStatusFilter, launchDateFilter } from '../../Helper/utils'

import './table-filter.css'

export default function TableFilter({
    filter,
    handleFilter
}:{
    handleFilter: ({
        launchStatus,
        launchDateFilter
    }:{
        launchStatus: string
        launchDateFilter: string
    }) => void;
    filter:{
        launchStatus: string;
        launchDateFilter: string;
    }
    setFilter: React.Dispatch<React.SetStateAction<{
        launchStatus: string;
        launchDateFilter: string;
    }>>
}) {

  const handleLaunchDateChange = (event: SelectChangeEvent) => {
    handleFilter({
            ...filter,
            launchDateFilter: event.target.value
        });
  };

  const handleLaunchStatusChange = (event: SelectChangeEvent) => {
    handleFilter({
        ...filter,
        launchStatus: event.target.value
    });

  };

  return (
    <div className='table-filter'>
        <div>
            <FormControl variant="standard" sx={{ m: 1, minWidth: 120 }}>
                <InputLabel>{launchDateFilter.all}</InputLabel>
                <Select
                value={filter.launchDateFilter}
                label="Age"
                onChange={handleLaunchDateChange}
                >
                {Object.keys(launchDateFilter).map((filterData, index)=>{
                    return <MenuItem key={`date-filter-${index}`} value={filterData}>{(launchDateFilter as any)[filterData]}</MenuItem>
                })}
                </Select>
            </FormControl>
        </div>
        <div>
            <FormControl variant="standard" sx={{ m: 1, minWidth: 120 }}>
                <InputLabel>{launchStatusFilter.all}</InputLabel>
                <Select
                value={filter.launchStatus}
                label="Age"
                onChange={handleLaunchStatusChange}
                >
                {Object.keys(launchStatusFilter).map((filterData, index)=>{
                    return <MenuItem key={`status-filter-${index}`} value={filterData}>{(launchStatusFilter as any)[filterData]}</MenuItem>
                })}
                </Select>
            </FormControl>
        </div>
    </div>
  );
}