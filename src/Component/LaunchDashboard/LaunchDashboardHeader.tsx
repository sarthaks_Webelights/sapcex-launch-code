
import SapcexLogo from '../../Assets/SpacexLogo'

import './launch-dashboard-header.css'

const LaunchDashboardHeader = () => {

  return (
      <div className="launch-dashboard-header">
        <div className="launch-dashboard-header-image-container">
            <SapcexLogo />
        </div>
      </div>
  );
}

export default LaunchDashboardHeader;