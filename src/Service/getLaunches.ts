const getAllSpacexLaunch = async (query: string) =>{
    const response = await fetch(`https://api.spacexdata.com/v3/launches?${query ? query : ""}`);
    const jsonData = await response.json();
    return jsonData
}

export {
    getAllSpacexLaunch
}