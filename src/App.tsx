import {
  QueryClient,
  QueryClientProvider,
} from '@tanstack/react-query'
import { StyledEngineProvider } from '@mui/material/styles';
import { BrowserRouter as Router, } from 'react-router-dom';

import LaunchDashboard from './Pages/LaunchDashboard'

import './App.css';

const App = () => {
  const queryClient = new QueryClient()

  return (
    <Router>
      <StyledEngineProvider injectFirst>

        <QueryClientProvider client={queryClient}>
          <LaunchDashboard />
        </QueryClientProvider>
      </StyledEngineProvider>
    </Router>
  );
}

export default App;
